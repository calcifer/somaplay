# somaplay

Play [soma.fm](https://somafm.com) stations easily from the commandline

Uses and requires `mpv` installed and in path

```
# Play station "groovesalad"
soma groovesalad

# Find stations with "groove" in the name
soma -s groove
```

# Installation and Setup

1. Make the share directory
   (one of `~/.local/share/soma`, `/usr/local/share/soma`, `/usr/share/soma`)
2. change to that directory
3. `/path/to/somaget`
4. Ensure you have `mpv` installed and in your path, along with appropriate 
   codecs. Your package manager is probably the easiest option here

# How it works

`somaget` enumerates the stations on the somafm website, and identifies the 
130kbps AAC stream playlist for each, then saves the url, description, and 
short name (ID) to a `.fm` file in the working directory.

`soma` uses the repository of `.fm` files in one of the share directories (see
*Installation and Setup* above for the list) to conduct its search and/or load
the playlist URL. When it gets an ID as an argument, it uses the first matching
`.fm` file it finds, extracts the URL for the playlist, and starts `mpv` with
that URL as the playlist argument
 
